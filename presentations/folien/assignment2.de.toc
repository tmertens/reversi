\beamer@endinputifotherversion {3.26pt}
\select@language {english}
\beamer@sectionintoc {1}{Networking}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Messages}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Message handling}{4}{0}{1}
\beamer@sectionintoc {2}{Rating function}{5}{0}{2}
\beamer@sectionintoc {3}{Miscellaneous changes}{6}{0}{3}
\beamer@subsectionintoc {3}{1}{Recoloring queue}{6}{0}{3}
\beamer@sectionintoc {4}{End}{9}{0}{4}
