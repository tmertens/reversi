\beamer@endinputifotherversion {3.20pt}
\select@language {german}
\beamer@sectionintoc {1}{UML Class diagramm}{3}{0}{1}
\beamer@sectionintoc {2}{Implementation}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Problems while implementing}{4}{0}{2}
\beamer@subsubsectionintoc {2}{1}{1}{Saving the map}{6}{0}{2}
\beamer@subsubsectionintoc {2}{1}{2}{Handling/saving the transitions}{7}{0}{2}
\beamer@subsubsectionintoc {2}{1}{3}{Check and execute a move}{8}{0}{2}
\beamer@sectionintoc {3}{Algorithm to rate a player's move}{9}{0}{3}
\beamer@subsectionintoc {3}{1}{Algorithm I}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{Algorithm II}{12}{0}{3}
\beamer@sectionintoc {4}{End}{14}{0}{4}
