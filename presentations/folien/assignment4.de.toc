\beamer@endinputifotherversion {3.20pt}
\select@language {english}
\beamer@sectionintoc {1}{Efficiency of pre-sorting}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Influence of map size}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Average time savings}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Consequences of benchmarking}{7}{0}{1}
\beamer@sectionintoc {2}{End}{8}{0}{2}
