package Reversi;

/**
 * Enumeration representing possible moves of a player.
 * 
 */
public enum StoneType {
	NORMAL, OVERWRITE, BOMB
}
