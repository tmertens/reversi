package Reversi;



import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import Benchmarks.Table;
import Message.*;

/**
 * Interprets messages from server and initiates new messages 
 * 
 */
public class MessageHandler {

	private GameClient client;
	private Game game;
	int moves=0;
	public Game getGame() {
		return game;
	}

	private Coordinate lastMove;

	private static final int group = 1;

	/**
	 * Constructor
	 * 
	 * @param client referenced game
	 */
	public MessageHandler(GameClient client) {

		this.client = client;

		init();

		listen();
	}

	/**
	 * initializes a game 
	 */
	private void init() {
		// Send group number
		byte[] message = { 1, 0, 0, 0, 1, group };
		client.sendByte(message);
		System.out.println("Sended group ID.");

		System.out.println("Waiting for map.");
		// Get Map
		Message map = client.readMessage();
		this.game = new Game(((MessageMap) map).getMap());
		System.out.println("Map received.");
		System.out.println(game);

		// Receive player number
		Message number = client.readMessage();
		Game.setOwnNumber(((MessageNumber) number).getNumber());
		System.out.println("Your Player number is " + Game.getOwnNumber()
				+ ".");
	}

	/**
	 * waiting for a server message
	 */
	public void listen() {
		while ((this.game.getDisqualified() || this.game.getEnded()) == false) {

			Message mes = client.readMessage();
			
			if(mes==null)
					this.game.setEnded(true);
			
			if (mes instanceof MessageRequest) {// MOVE REQUEST
				
				if(Reversi.automatic){
					AI.Minimax.setTimePerMove(1200000);
					// Start minimax search
					AI.Minimax.minimax(game.getMap(), Reversi.depth);
				}else{
					
					if(((MessageRequest) mes).getTime() != 0){
						AI.Minimax.setTimePerMove(((MessageRequest) mes).getTime());
						// Start minimax search
						if(Game.getPhase()==1){
							AI.Minimax.minimax(game.getMap(), 25);
						}else{
							AI.Minimax.minimax(game.getMap(), 1);
						}
					}else{	
						AI.Minimax.setTimePerMove(1200000000);
						// Start minimax search
						AI.Minimax.minimax(game.getMap(), ((MessageRequest) mes).getDepth());
					}
				}
				
				// Search for possible move
				Coordinate possible = AI.Minimax.getBestMove();
								
				if (possible != null) {
					byte x = (byte) possible.getX();
					byte y = (byte) possible.getY();
					byte c = 0;

					if (this.game.getMap().getField(possible.getX(), possible.getY()).getState() == FieldState.BONUS) {
						c = Game.CHOICE_OVERWRITESTONE; //Default: OverWriteStone
					} else if (this.game.getMap().getField(possible.getX(), possible.getY()).getState() == FieldState.CHOICE) {
						c = (byte) AI.AI.getBestChoice(this.game.getMap(), Game.getOwnNumber()); //Search best player to change
					}

					byte[] message = { 5, 0, 0, 0, 5, 0, x, 0, y, c };
					client.sendByte(message); // Send move
					lastMove=possible;
				}
				
				if (Reversi.benchmarkingEnabled) {
					Table.getInstance().submitMove();
				}

			} else if (mes instanceof MessageMove) {// EXECUTED MOVE
				
				int x = ((MessageMove) mes).getPosX();
				int y = ((MessageMove) mes).getPosY();
				int player = ((MessageMove) mes).getPlayer();
				int choice = ((MessageMove) mes).getChoice();
				
				game.makeMove(x, y, player, choice);
				
				System.out.println("move "+moves+": player "+player+" moved ("+x+","+y+","+choice+"). time left: 0 msecs");	
				moves++;
			} else if (mes instanceof MessageDisqualification) {// PLAYER DISQUALICATION
				if(((MessageDisqualification)mes).getNumber()==Game.getOwnNumber()){
					game.setDisqualified(true);// Stop listening
					System.out.println(game.getMap());
					System.out.println("OVER:"+Game.getPlayer(Game.getOwnNumber()).getOverWriteStones()+"BOMBS:"+Game.getPlayer(Game.getOwnNumber()).getBombs());
					System.out.println("Last Move before Disqualication: "+lastMove);
				}
				System.out.println("Player " + ((MessageDisqualification) mes).getNumber() + " is disqualified.");
			} else if (mes instanceof MessageEnd) {// GAME END
				game.setEnded(true);// Stop listening
				System.out.println("Final map:");
				System.out.println(game);
				System.out.println("Game finished.");
					
				//Endfile erzeugen
				if(Reversi.benchmarkingEnabled){
				    try {
				    	FileWriter fw=new FileWriter("ende.txt");
					    BufferedWriter bw = new BufferedWriter(fw);
						bw.write("test test test");
						bw.write("tset tset tset");
					    bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}	
			} else if (mes instanceof MessagePhase) {// PHASE END
				Game.setPhase(2);
				System.out.println("Begin of phase 2");
			}
		}
	}

}
