package Reversi;

/**
 * Represents one field on the map.
 * 
 */
public class Field {

	private int posX;
	private int posY;
	private FieldState state;
	private Field[] neighbours;
	private int[] direction;

	/**
	 * Constructor for Field. It is mostly called by the map constructor.
	 * Neighbors are initially filled with null. The actual neighbors are
	 * assigned later.
	 * 
	 * @param givenState
	 *            State as read from file
	 * @param x
	 *            x coordinate of the field in the map
	 * @param y
	 *            y coordinate of the field in the map
	 */
	public Field(String givenState, int x, int y) {

		state = FieldState.BLOCKED;

		if (givenState.equals("0")) {
			state = FieldState.NORMAL;
		} else if (givenState.equals("1")) {
			state = FieldState.PLAYER1;
		} else if (givenState.equals("2")) {
			state = FieldState.PLAYER2;
		} else if (givenState.equals("3")) {
			state = FieldState.PLAYER3;
		} else if (givenState.equals("4")) {
			state = FieldState.PLAYER4;
		} else if (givenState.equals("5")) {
			state = FieldState.PLAYER5;
		} else if (givenState.equals("6")) {
			state = FieldState.PLAYER6;
		} else if (givenState.equals("7")) {
			state = FieldState.PLAYER7;
		} else if (givenState.equals("8")) {
			state = FieldState.PLAYER8;
		} else if (givenState.equals("-")) {
			state = FieldState.BLOCKED;
		} else if (givenState.equals("c")) {
			state = FieldState.CHOICE;
		} else if (givenState.equals("b")) {
			state = FieldState.BONUS;
		} else if (givenState.equals("x")) {
			state = FieldState.EXPANSION;
		} else if (givenState.equals("i")) {
			state = FieldState.INVERSION;
		}

		neighbours = new Field[8];
		setPosX(x);
		setPosY(y);

		direction = new int[8];
		for (int i = 0; i < 8; i++) {
			direction[i] = i;
		}
	}

	/**
	 * Used to determine validity of moves and token ownership
	 * 
	 * @return Owner of the stone in this field. If it not owned by any players,
	 *         0 is returned.
	 */
	public int getPlayer() {
		switch (this.state) {
		case PLAYER1:
			return 1;
		case PLAYER2:
			return 2;
		case PLAYER3:
			return 3;
		case PLAYER4:
			return 4;
		case PLAYER5:
			return 5;
		case PLAYER6:
			return 6;
		case PLAYER7:
			return 7;
		case PLAYER8:
			return 8;
		default:
			return 0;
		}
	}

	/**
	 * Sets the state of the field to the given player
	 * 
	 * @param player
	 *            new player placing a stone on this field
	 */
	public void setPlayer(int player) {
		switch (player) {
		case 1:
			this.state = FieldState.PLAYER1;
			break;
		case 2:
			this.state = FieldState.PLAYER2;
			break;
		case 3:
			this.state = FieldState.PLAYER3;
			break;
		case 4:
			this.state = FieldState.PLAYER4;
			break;
		case 5:
			this.state = FieldState.PLAYER5;
			break;
		case 6:
			this.state = FieldState.PLAYER6;
			break;
		case 7:
			this.state = FieldState.PLAYER7;
			break;
		case 8:
			this.state = FieldState.PLAYER8;
			break;
		default:
		}
	}

	/**
	 * Used to handle a change of direction after a transition
	 * 
	 * @param oldDir
	 *            old direction of the transition
	 * @param newDir
	 *            new direction of the transition
	 */
	public void setDirection(int oldDir, int newDir) {
		this.direction[oldDir] = newDir;
	}

	/**
	 * Used to handle a change of direction after a transition
	 * 
	 * @param dir
	 * @return the new direction after a transition (or a normal move)
	 */
	public int getDirection(int dir) {
		return (direction[dir]);
	}
	
	/**
	 * @param direction
	 *            the direction of the neighbour between 0 and 8
	 * @return the neighbour of the field in the direction
	 */
	public Field getNeighbour(int direction) {
		return neighbours[direction];
	}

	/**
	 * @param direction
	 *            the direction of the neighbour between 0 and 8
	 * @param neighbour
	 *            the new neighbour of the field in the given direction
	 */
	public void setNeighbour(int direction, Field neighbour) {
		this.neighbours[direction] = neighbour;
	}
	
	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public FieldState getState() {
		return state;
	}

	public void setState(FieldState state) {
		this.state = state;
	}
	
	/**
	 * Graphical representation of a field for console output of maps. In case
	 * of an unknown token, "ERROR" is returned to alert the user.
	 */
	public String toString() {
		switch (this.state) {
		case PLAYER1:
			return "1";
		case PLAYER2:
			return "2";
		case PLAYER3:
			return "3";
		case PLAYER4:
			return "4";
		case PLAYER5:
			return "5";
		case PLAYER6:
			return "6";
		case PLAYER7:
			return "7";
		case PLAYER8:
			return "8";
		case NORMAL:
			return "0";
		case BLOCKED:
			return "-";
		case CHOICE:
			return "c";
		case INVERSION:
			return "i";
		case BONUS:
			return "b";
		case EXPANSION:
			return "x";
		}
		return "ERROR";
	}
}
