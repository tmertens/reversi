package Reversi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import AI.Minimax;
import Benchmarks.Table;
import Benchmarks.TableManager;

/**
 * Controls the game/AI and includes the main function.
 * 
 */
public class Reversi {

	private static GameClient client;
	private static MessageHandler handler;

	// DEFINES
	public static boolean alphaBetaEnabled = true;
	public static boolean preSortingMoves = true;
	public static boolean benchmarkingEnabled = false;
	public static String name = "";
	public static boolean automatic = false;
	public static boolean ignoreTime = false;
	public static int depth = 2;
	public static int index = 0;
	
	/**
	 * Controls the game and AI
	 * 
	 * @param args
	 *            options such as map filename or help option
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to Reversi+X!");

		// Create table
		if (Reversi.benchmarkingEnabled) {
			new Table();
		}

		if (args.length > 0) {
			String option = args[0];

			if (option.equals("-h")) {
				System.out.println(displayHelp());
			} else if (option.equals("-s")) {
				if (args.length > 1) {
					try {
						client = new GameClient(args[1],
								Integer.parseInt(args[2]));
						setHandler(new MessageHandler(client));
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println(displayHelp());
				}
			} else if (option.equals("-t")) {
				if (args.length > 1) {
					benchmarkingEnabled = Boolean.parseBoolean(args[1]);
					if (benchmarkingEnabled)
						new Table();
					alphaBetaEnabled = Boolean.parseBoolean(args[2]);
					preSortingMoves = Boolean.parseBoolean(args[3]);
					depth = Integer.parseInt(args[4]);
					index = Integer.parseInt(args[5]);
					automatic = true;

					System.out.println("AlphaBeta:" + alphaBetaEnabled);
					System.out.println("PreSorting:" + preSortingMoves);

					try {
						client = new GameClient("localhost", 7777);
						setHandler(new MessageHandler(client));

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					name = "m" + index + "-"
							+ handler.getGame().getMap().getSizeX() + "x"
							+ handler.getGame().getMap().getSizeY() + "-"
							+ depth + "-" + alphaBetaEnabled + "-"
							+ preSortingMoves;
				}
			}
		} else {
			// User didn't give us any information -> they need help
			System.out.println(displayHelp());
			try {
			  // TEST without Server
			  String text=""; 
			  BufferedReader input = new BufferedReader(new FileReader("maps/15x15.txt")); String line;
		
			  while ((line = input.readLine()) != null) {
				  	if(text!="")text =text + '\n' + line; 
				  	if(text=="")text = line; 
			  } 
			  if(text.endsWith("\n")) 
				  text = text + "\n";
			  
			  input.close();
			  Game.setOwnNumber(2); 
			  Game game=new Game(text);   
			  
			  System.out.println(game.getMap().reachableInversion());
			  System.out.println(game);
			  
			  long zstVorher;
			  long zstNachher;
			  
			  zstVorher = System.currentTimeMillis();
			  AI.Minimax.setTimePerMove(2000); 
			  Minimax.minimax(game.getMap(), 100);
			  zstNachher = System.currentTimeMillis();
			  
			  System.out.println("player: "+Minimax.getFinalPlayer());
			  System.out.println("Zeit: " + ((zstNachher - zstVorher)) + " msec");
			  System.out.println("End"); 
			  System.out.println(Minimax.getBestMove().getX() +","+Minimax.getBestMove().getY()); System.out.println("Zeit: " +
			  ((zstNachher - zstVorher)) + " msec");
			  
			  // EXECUTION with Server 
			  /* 
			  client = new GameClient("localhost", 7777); 
			  setHandler(new MessageHandler(client));
			 */
			  }
			
			  //Exceptions
			 catch (FileNotFoundException e) {
			  e.printStackTrace(); 
			 } catch (IOException e) {
			  e.printStackTrace();
			 } 
		}
		// Write table to file
		if (Reversi.benchmarkingEnabled) {
			TableManager manager = new TableManager(Table.getInstance());
			manager.writeTableToFile();
		}
	}

	/**
	 * Displays a help on the screen
	 * 
	 * @return help as String
	 */
	private static String displayHelp() {
		String help = new String();

		help += "---------------------------------------\n";
		help += "--   Reversi+X Heuristic Algorithm   --\n";
		help += "--                                   --\n";
		help += "-- options:                          --\n";
		help += "--   -h       displays this help     --\n";
		help += "--   -s       serveradress port	  --\n";
		help += "--                                   --\n";
		help += "---------------------------------------\n";

		return help;
	}

	
	public static MessageHandler getHandler() {
		return handler;
	}

	public static void setHandler(MessageHandler handler) {
		Reversi.handler = handler;
	}
	
	public static boolean isPreSortingMoves() {
		return preSortingMoves;
	}

	public static void setPreSortingMoves(boolean preSortingMoves) {
		Reversi.preSortingMoves = preSortingMoves;
	}

	public static boolean isAlphaBetaEnabled() {
		return alphaBetaEnabled;
	}

	public static void setAlphaBetaEnabled(boolean alphaBetaEnabled) {
		Reversi.alphaBetaEnabled = alphaBetaEnabled;
	}

}
