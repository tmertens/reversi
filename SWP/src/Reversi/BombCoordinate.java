package Reversi;

/**
 * 
 * Used to store information about visited fields in the bomb phase
 */
public class BombCoordinate {

	private int x;
	private int y;
	private int stepsLeft;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @param stepsLeft
	 *            remaining recursive steps
	 */
	public BombCoordinate(int x, int y, int stepsLeft) {
		this.x = x;
		this.y = y;
		this.stepsLeft = stepsLeft;
	}

	
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getStepsLeft() {
		return stepsLeft;
	}

	public void setStepsLeft(int stepsLeft) {
		this.stepsLeft = stepsLeft;
	}

	
	public boolean equals(Object o) {
		if (((BombCoordinate) o).getX() == this.getX()
				&& ((BombCoordinate) o).getY() == this.getY()
				&& ((BombCoordinate) o).getStepsLeft() >= this.getStepsLeft())
			return true;
	
		return false;
	}

}
