package Reversi;

import java.util.*;

/**
 * Represents the whole map as a 1-dimensional array.
 * 
 */
public class Map {

	private int sizeX;
	private int sizeY;
	private int players;
	private int bombs;
	private int bombSize;
	private int overwrite;

	private static int[] stoneCount;
	private static LinkedList<BombCoordinate> visitedBomb;

	private static LinkedList<Coordinate> safeStones;
	private static LinkedList<Coordinate> halfSafeStones;

	private Field fieldsOneDim[];
	private HashSet<Coordinate> recoloringQueue;

	/**
	 * Constructor for Map. Reading from file and writing into map data
	 * structure happens here. The top left of the map is (0,0) whereas the
	 * bottom right is (sizeX-1, sizeY-1).
	 * 
	 * @param filename
	 *            Filename of the map file to be loaded
	 * @param playersArray
	 *            Array of player from game to store initial stone information
	 */
	public Map(String map) {

		recoloringQueue = new HashSet<Coordinate>();
		visitedBomb = new LinkedList<BombCoordinate>();

		map.replaceAll("\r\n", "\n");
		String[] mapArray = map.split("\n");
		String[] mapHelp;

		// index indicates what section of the file is being processed
		int index = 1;
		int lineNumber = 0;
		String line = null;
		while (lineNumber < mapArray.length) {
			line = mapArray[lineNumber];
			switch (index) {
			case 1:
				// Read number of players
				setPlayers(Integer.parseInt(line));
				break;
			case 2:
				// Read number of overwrite stones
				mapHelp = line.split(" ");
				setOverwrite(Integer.parseInt(line));
				break;
			case 3:
				// Read bomb information
				mapHelp = line.split(" ");
				setBombs(Integer.parseInt(mapHelp[0]));
				setBombSize(Integer.parseInt(mapHelp[1]));
				break;
			case 4:
				// Read map dimensions
				mapHelp = line.split(" ");
				setSizeY(Integer.parseInt(mapHelp[0]));
				setSizeX(Integer.parseInt(mapHelp[1]));
				fieldsOneDim = new Field[getSizeX() * getSizeY()];
				break;
			case 5:
				// Read fields of map
				index = 6;
				for (int y = 0; y < getSizeY(); y++) {
					mapHelp = line.split(" ");
					for (int x = 0; x < getSizeX(); x++) {
						setField(x, y, new Field(mapHelp[x], x, y));
					}
					lineNumber++;
					if (lineNumber < mapArray.length)
						line = mapArray[lineNumber];
				}

				// Set Standard Neighbors
				setStandardNeighbours();

				int ax,
				ay,
				ad,
				bx,
				by,
				bd;

				// Read transitions
				while (lineNumber < mapArray.length) {
					line = mapArray[lineNumber];
					mapHelp = line.split(" ");

					ax = Integer.parseInt(mapHelp[0]);
					ay = Integer.parseInt(mapHelp[1]);
					ad = Integer.parseInt(mapHelp[2]);
					bx = Integer.parseInt(mapHelp[4]);
					by = Integer.parseInt(mapHelp[5]);
					bd = Integer.parseInt(mapHelp[6]);

					getField(ax, ay).setNeighbour(ad, getField(bx, by));
					getField(bx, by).setNeighbour(bd, getField(ax, ay));
					getField(ax, ay).setDirection(ad, ((bd + 4) % 8));
					getField(bx, by).setDirection(bd, ((ad + 4) % 8));

					lineNumber++;
				}
			}
			index++;
			lineNumber++;
		}

		// Initialize safeStone count and normal stoneCount
		safeStones = new LinkedList<Coordinate>();
		halfSafeStones = new LinkedList<Coordinate>();
		stoneCount = new int[getPlayers() + 1];

		for (int x = 0; x < getSizeX(); x++) {
			for (int y = 0; y < getSizeY(); y++) {
				if (this.countNullNeighbors(x, y) >= 4) {
					Map.getSafeStones().add(new Coordinate(x, y));
				} else if (countNullNeighbors(x, y) == 3) {
					Map.getHalfSafeStones().add(new Coordinate(x, y));
				}

				stoneCount[getField(x, y).getPlayer()]++;
			}
		}

	}

	/**
	 * Counts number of null neighbors for the field (x,y)
	 * 
	 * @param map
	 *            map
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @return number of null neighbors (- on the map)
	 */
	public int countNullNeighbors(int x, int y) {
		int numOfNullNeighbors = 0;
		for (int i = 0; i < 8; i++) {
			if ((getFieldNeighbour(x, y, i) == null)||(getFieldNeighbour(x,y,i).getState() == FieldState.BLOCKED)) {
				numOfNullNeighbors++;
			}
		}

		return numOfNullNeighbors;
	}

	/**
	 * Sets neighbors of each field according to map structure, i.e. as if there
	 * were no transitions. Fields bordering empty field (-) are assigned null
	 * as a neighbor for the bordering side.
	 * 
	 */
	public void setStandardNeighbours() {

		for (int y = 0; y < getSizeY(); y++) {
			for (int x = 0; x < getSizeX(); x++) {
				for (int i = 0; i < 8; i++) {
					switch (i) {
					case 0:
						if (y > 0) {
							getField(x, y).setNeighbour(i, getField(x, y - 1));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 1:
						if (y > 0 && x < getSizeX() - 1) {
							getField(x, y).setNeighbour(i,
									getField(x + 1, y - 1));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 2:
						if (x < getSizeX() - 1) {
							getField(x, y).setNeighbour(i, getField(x + 1, y));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 3:
						if (y < getSizeY() - 1 && x < getSizeX() - 1) {
							getField(x, y).setNeighbour(i,
									getField(x + 1, y + 1));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 4:
						if (y < getSizeY() - 1) {
							getField(x, y).setNeighbour(i, getField(x, y + 1));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 5:
						if (y < getSizeY() - 1 && x > 0) {
							getField(x, y).setNeighbour(i,
									getField(x - 1, y + 1));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 6:
						if (x > 0) {
							getField(x, y).setNeighbour(i, getField(x - 1, y));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					case 7:
						if (y > 0 && x > 0) {
							getField(x, y).setNeighbour(i,
									getField(x - 1, y - 1));
						} else {
							getField(x, y).setNeighbour(i, null);
						}
						break;
					}
				}
			}
		}
	}


	/**
	 * Swaps stones of playerA and playerB
	 * 
	 * @param playerA
	 *            playerA, the first player
	 * @param playerB
	 *            playerB, the second player
	 */
	public void swapStones(int playerA, int playerB) {
		Field field;
		for (int x = 0; x < getSizeX(); x++) {
			for (int y = 0; y < getSizeY(); y++) {
				field = getField(x, y);

				// Update stoneCount
				stoneCount[field.getPlayer()]--;

				if (field.getPlayer() == playerA) {
					field.setPlayer(playerB);
				} else if (field.getPlayer() == playerB) {
					field.setPlayer(playerA);
				}

				// Update stoneCount
				stoneCount[field.getPlayer()]++;
			}
		}
	}

	/**
	 * undo swap operation
	 * 
	 * @param playerA
	 *            stonecolor one
	 * @param playerB
	 *            stonecolor two
	 */
	public void undoSwapStones(int playerA, int playerB) {
		swapStones(playerA, playerB);
	}

	/**
	 * Rotates stones, so that stones of player k are stones of player (k%
	 * MAXPLAYERS )+1 after the inversion.
	 */
	public void invertStones() {
		Field field;
		for (int x = 0; x < getSizeX(); x++) {
			for (int y = 0; y < getSizeY(); y++) {
				field = getField(x, y);
				if (field.getPlayer() != 0) {

					// Update stoneCount
					stoneCount[getField(x, y).getPlayer()]--;

					field.setPlayer((field.getPlayer() % getPlayers()) + 1);

					// Update stoneCount
					stoneCount[getField(x, y).getPlayer()]++;

				}
			}
		}
	}

	/**
	 * Reverses an invert field
	 */
	public void undoInvertStones() {
		Field field;
		for (int x = 0; x < getSizeX(); x++) {
			for (int y = 0; y < getSizeY(); y++) {
				field = getField(x, y);
				if (field.getPlayer() != 0) {

					// Update stoneCount
					stoneCount[field.getPlayer()]--;

					int newPlayer = field.getPlayer() - 1;
					if (newPlayer == 0)
						newPlayer = getPlayers();
					field.setPlayer(newPlayer);

					// Update stoneCount
					stoneCount[field.getPlayer()]++;
				}
			}
		}
	}



	/**
	 * Makes move taking into account the different scenarios in which a move
	 * can happen. Calls makeMoveNormal, makeMoveBomb and handles special
	 * stones.
	 * 
	 * @param x
	 *            x position of new stone
	 * @param y
	 *            y position of new stone
	 * @param player
	 *            player doing the turn
	 * @param type
	 *            type of the new stone
	 * @param choice
	 *            opponent player that player player wants to swap stones with
	 * @return true, if the move is successfully executed (including validation)
	 */
	public undoInformation makeMove(int x, int y, int player, int choice) {
	
		// Check StoneType
		StoneType type = StoneType.NORMAL;
		if ((getField(x, y).getPlayer() != 0)
				|| (getField(x, y).getState() == FieldState.EXPANSION)) {
			type = StoneType.OVERWRITE;
		}
		if (Game.getPhase() == 2)
			type = StoneType.BOMB;
	
		// Clear recoloringQueue
		recoloringQueue.clear();
	
		// Save all undo information
		undoInformation undo = new undoInformation(getPlayers(), Game
				.getPlayer(Game.getOwnNumber()).getOverWriteStones());
	
		switch (type) {
		case NORMAL:
			if (getField(x, y).getState() == FieldState.NORMAL) {
				undo.setMoveList(makeMoveNormal(x, y, player));
			} else if (getField(x, y).getState() == FieldState.CHOICE) {
				undo.setMoveList(makeMoveNormal(x, y, player));
				if (choice == -1)
					choice = player;
				swapStones(player, choice);
				undo.setChoice(true);
				undo.setChoiceA(player);
				undo.setChoiceB(choice);
			} else if (getField(x, y).getState() == FieldState.INVERSION) {
				undo.setMoveList(makeMoveNormal(x, y, player));
				invertStones();
				undo.setInversion(true);
			} else if (getField(x, y).getState() == FieldState.BONUS) {
				if (choice == -1)
					choice = Game.CHOICE_OVERWRITESTONE;
				if (choice == Game.CHOICE_BOMB) {
					Game.getPlayer(player).increaseBombs();
				} else if (choice == Game.CHOICE_OVERWRITESTONE) {
					Game.getPlayer(player).increaseOverWriteStones();
				}
				undo.setMoveList(makeMoveNormal(x, y, player));
			} else if (getField(x, y).getState() == FieldState.EXPANSION) {
				enqueueForRecoloring(x, y);
				undo.setMoveList(makeMoveNormal(x, y, player));
			} else {
				// Execute normal move
				undo.setMoveList(makeMoveNormal(x, y, player));
			}
	
			break;
		case OVERWRITE:
			Game.getPlayer(player).decreaseOverWriteStones();
			if (getField(x, y).getState() == FieldState.EXPANSION)
				enqueueForRecoloring(x, y);
			undo.setMoveList(makeMoveNormal(x, y, player));
			break;
		case BOMB:
			visitedBomb.clear();
			Game.getPlayer(player).decreaseBombs();
			makeMoveBomb(x, y, getBombSize());
			undo.setMoveList(recolorQueuedCoordinates(0));
			break;
		}	
		return undo;
	}

	/**
	 * Makes normal move, i.e. a move with a normal stone on an empty field
	 * 
	 * @param x
	 *            x position of new stone
	 * @param y
	 *            y position of new stone
	 * @param playerStoneColor
	 *            current stone color of the player
	 * @return
	 */
	public HashSet<undoField> makeMoveNormal(int x, int y, int player) {
	
		for (int i = 0; i < 8; i++) {
			moveDir(x, y, player, i, 0, 1);
		}
	
		return recolorQueuedCoordinates(player);
	}

	/**
	 * Places a bomb at (x,y). All fields reachable from (x,y) within bombSize
	 * steps will be destroyed (turned into BLOCKED). One bomb will be deducted
	 * from players 'inventory'.
	 * 
	 * @param x
	 *            x position of new stone
	 * @param y
	 *            y position of new stone
	 * @param stepsLeft
	 *            steps left to go. Initial value is bombSize.
	 */
	public void makeMoveBomb(int x, int y, int stepsLeft) {

		// Check if field is already blocked
		if (getField(x, y).getState() == FieldState.BLOCKED)
			return;

		BombCoordinate bc = new BombCoordinate(x, y, stepsLeft);

		if (visitedBomb.contains(bc))
			return;
		visitedBomb.add(bc);

		enqueueForRecoloring(x, y);

		// Check stepsLeft>0
		if (stepsLeft <= 0)
			return;

		// Decrease stepsLeft
		stepsLeft--;

		// Place bombs with stepsLeft at neighbor positions
		for (int dir = 0; dir < 8; dir++) {
			Field fieldNeighbour = getFieldNeighbour(x, y, dir);
			if (fieldNeighbour != null) {
				makeMoveBomb(fieldNeighbour.getPosX(),
						fieldNeighbour.getPosY(), stepsLeft);
			}
		}

	}

	/**
	 * Attempts to move in a specific direction. Used for testing possible
	 * moves.
	 * 
	 * @param x
	 *            x position of the new stone
	 * @param y
	 *            y position of the new stone
	 * @param playerStoneColor
	 *            current stone color of the player
	 * @param dir
	 *            direction to move in
	 * @param counter
	 *            recursion depth
	 * @param mode
	 *            Controls the behavior of the method. 0 is testing mode, 1 is
	 *            writing mode
	 * @return true on success, false otherwise
	 */
	public boolean moveDir(int x, int y, int player, int dir, int counter,
			int mode) {
	
		boolean isValid = false;
		Field fieldNeighbour;
		int safeX = x;
		int safeY = y;
		int safeDir = dir;
		LinkedList<Coordinate> visited = new LinkedList<Coordinate>();
		Coordinate c;
		visited.add(new Coordinate(x, y));
	
		while (true) {
	
			fieldNeighbour = getFieldNeighbour(x, y, dir);
	
			if (fieldNeighbour == null)
				break;
	
			c = new Coordinate(fieldNeighbour.getPosX(),
					fieldNeighbour.getPosY(), dir);
	
			if (visited.contains(c))
				return false;
			visited.add(c);
	
			if (fieldNeighbour.getPlayer() == player) {
				// minimum 1 other stone between the stone on this field and the
				// new one
				if (counter == 0)
					break;

				isValid = true;
				break;
			} else if (fieldNeighbour.getPlayer() > 0
					|| (fieldNeighbour.getState() == FieldState.EXPANSION)) {
				
			} else {
				break;
			}
	
			dir = getField(x, y).getDirection(dir);
			x = fieldNeighbour.getPosX();
			y = fieldNeighbour.getPosY();
	
			counter++;
		}
	
		if (mode == 1 && isValid) {
			x = safeX;
			y = safeY;
			dir = safeDir;
	
			while (true) {
				fieldNeighbour = getFieldNeighbour(x, y, dir);
	
				if (fieldNeighbour.getPlayer() == player) {
					enqueueForRecoloring(x, y);
					break;
				} else if (fieldNeighbour.getPlayer() > 0
						|| (fieldNeighbour.getState() == FieldState.EXPANSION)) {
					enqueueForRecoloring(x, y);
				}
	
				dir = getField(x, y).getDirection(dir);
				x = fieldNeighbour.getPosX();
				y = fieldNeighbour.getPosY();
			}
		}
	
		return isValid;
	}

	/**
	 * Reverses a makeMove operation
	 * 
	 * @param undo
	 *            saved undoInformation when executing function makeMove
	 */
	public void undoMove(undoInformation undo) {

		if (undo.isChoice()) { // Undo choice field
			undoSwapStones(undo.getChoiceA(), undo.getChoiceB());
		} else if (undo.isInversion()) { // Undo inversion field
			undoInvertStones();
		}

		// Reset overwritestones and bombs
		for (int i = 1; i <= getPlayers(); i++) {
			Game.getPlayer(i).setOverWriteStones(
					undo.getOldOverWriteStones()[i]);
			Game.getPlayer(i).setBombs(undo.getOldBombs()[i]);
		}
		// Reset all fields
		Iterator<undoField> iter = undo.getMoveList().iterator();
		while (iter.hasNext()) {

			undoField resetField = iter.next();

			// Update stoneCount
			stoneCount[getField(resetField.getPosX(), resetField.getPosY())
					.getPlayer()]--;

			getField(resetField.getPosX(), resetField.getPosY()).setState(
					resetField.getOldState());

			// Update stoneCount
			stoneCount[getField(resetField.getPosX(), resetField.getPosY())
					.getPlayer()]++;

			iter.remove();
		}

	}

	/**
	 * Enqueues a coordinate for recoloring. All coordinates are assumed to be
	 * targeted at being recolored to the same color.
	 * 
	 * @param x
	 *            x coordinate in map
	 * @param y
	 *            y coordinate in map
	 */
	public void enqueueForRecoloring(int x, int y) {
		Coordinate c = new Coordinate(x, y);
		recoloringQueue.add(c);
	}

	/**
	 * Recolors all queued up fields. Flushes recoloringQueue afterwards.
	 * 
	 * @param newColor
	 *            target color for coordinates
	 * @return
	 */
	public HashSet<undoField> recolorQueuedCoordinates(int newColor) {

		HashSet<undoField> moveList = new HashSet<undoField>();

		Iterator<Coordinate> iter = recoloringQueue.iterator();
		while (iter.hasNext()) {
			Coordinate c = iter.next();

			// Save current field (for undo move)
			moveList.add(new undoField(c.getX(), c.getY(), getField(c.getX(),
					c.getY()).getState()));

			// Update stoneCount
			stoneCount[getField(c.getX(), c.getY()).getPlayer()]--;

			// Recolor field
			if (newColor == 0) {
				getField(c.getX(), c.getY()).setState(FieldState.BLOCKED);
			} else {
				getField(c.getX(), c.getY()).setPlayer(newColor);
			}

			// Update stoneCount
			stoneCount[getField(c.getX(), c.getY()).getPlayer()]++;

			// Remove c from recoloringQueue using iterator
			iter.remove();
		}

		return moveList;
	}
	
	/**
	 * Calculates the amount of Inversionfields that can be reached, this means fields with more than 2 neighbours
	 * @return the amout of reachable Inversionfields
	 */
	public int reachableInversion(){
		int result = 0;
		for (int x=0 ; x < getSizeX(); x++){
			for (int y=0 ; y < getSizeY(); y++){
				if(getField(x,y).getState()==FieldState.INVERSION){
					if(countNullNeighbors(x,y) <= 6)
						result++;
				}
			}
		}
		return result;
	}

	/**
	 * Allows for easy traversing through the map.
	 * 
	 * @param x
	 *            x coordinate of starting position
	 * @param y
	 *            y coordinate of starting position
	 * @param dir
	 *            direction of travel
	 * @return Neighbor of field at (x,y) in direction dir. Takes into account
	 *         transitions
	 */
	public Field getFieldNeighbour(int x, int y, int dir) {
		return this.getField(x, y).getNeighbour(dir);
	}

	public int getPlayers() {
		return players;
	}

	public void setPlayers(int players) {
		this.players = players;
	}
	
	public static LinkedList<Coordinate> getSafeStones() {
		return safeStones;
	}

	public static void setSafeStones(LinkedList<Coordinate> safeStones) {
		Map.safeStones = safeStones;
	}

	public static LinkedList<Coordinate> getHalfSafeStones() {
		return halfSafeStones;
	}

	public static void setHalfSafeStones(LinkedList<Coordinate> halfSafeStones) {
		Map.halfSafeStones = halfSafeStones;
	}

	public static int[] getStoneCount() {
		return stoneCount;
	}

	public static void setStoneCount(int[] stoneCount) {
		Map.stoneCount = stoneCount;
	}

	public static int getStoneCount(int player) {
		return stoneCount[player];
	}

	private void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	private void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}
	
	public Field getField(int x, int y) {
		return fieldsOneDim[y * this.getSizeX() + x];
	}

	public void setField(int x, int y, Field field) {
		fieldsOneDim[y * this.getSizeX() + x] = field;
	}
	
	public int getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(int overwrite) {
		this.overwrite = overwrite;
	}

	public int getBombSize() {
		return bombSize;
	}

	public void setBombSize(int bombSize) {
		this.bombSize = bombSize;
	}

	public int getBombs() {
		return bombs;
	}

	public void setBombs(int bombs) {
		this.bombs = bombs;
	}
	
	public int getSizeY() {
		return sizeY;
	}

	public int getSizeX() {
		return sizeX;
	}
	
	public String toString() {
		String result = "";
		for (int y = 0; y < getSizeY(); y++) {
			for (int x = 0; x < getSizeX(); x++) {
				result = result + getField(x, y).toString() + " ";
			}
			result = result + "\n";
		}
		return result;	
	}
}
