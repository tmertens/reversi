package Reversi;

import java.util.*;

/**
 * Stores the undo information of a makeMove operation
 *
 */
public class undoInformation {

	private HashSet<undoField> moveList;
	private boolean choice;
	private int choiceA;
	private int choiceB;
	private boolean inversion;
	private int[] oldOverWriteStones;
	private int[] oldBombs;
	private int amount;
	
	/**
	 * Constructor
	 *  
	 */
	public undoInformation(int players, int amount){
		this.moveList=new HashSet<undoField>();
		
		this.choice=false;
		if(choice){
			this.choiceA=0;
			this.choiceB=0;
		}
		this.inversion=false;
		
		this.oldOverWriteStones=new int[players+1];
		this.oldBombs=new int[players+1];
		
		this.setAmount(amount);
		
		// Initialize old overwritestones and old bombs
		for(int i=1; i<=players; i++){
			this.oldOverWriteStones[i]=Game.getPlayer(i).getOverWriteStones();
			this.oldBombs[i]=Game.getPlayer(i).getBombs();
		}	
	}
	
	/**
	 * Constructor
	 *  
	 */
	public undoInformation(HashSet<undoField> moveList, boolean choice, int choiceA, int choiceB, boolean inversion, int players){
		this.moveList=new HashSet<undoField>();
		this.moveList=moveList;
		
		this.choice=choice;
		if(choice){
			this.choiceA=choiceA;
			this.choiceB=choiceB;
		}
		this.inversion=inversion;
		
		this.oldOverWriteStones=new int[players+1];
		this.oldBombs=new int[players+1];
		
		// Initialize old overwritestones and old bombs
		for(int i=1; i<=players; i++){
			this.oldOverWriteStones[i]=Game.getPlayer(i).getOverWriteStones();
			this.oldBombs[i]=Game.getPlayer(i).getBombs();
		}	
	}
	
	
	public HashSet<undoField> getMoveList() {
		return moveList;
	}

	public void setMoveList(HashSet<undoField> moveList) {
		this.moveList = moveList;
	}

	public boolean isChoice() {
		return choice;
	}

	public void setChoice(boolean choice) {
		this.choice = choice;
	}

	public int getChoiceA() {
		return choiceA;
	}

	public void setChoiceA(int choiceA) {
		this.choiceA = choiceA;
	}

	public int getChoiceB() {
		return choiceB;
	}

	public void setChoiceB(int choiceB) {
		this.choiceB = choiceB;
	}

	public boolean isInversion() {
		return inversion;
	}

	public void setInversion(boolean inversion) {
		this.inversion = inversion;
	}

	public int[] getOldOverWriteStones() {
		return oldOverWriteStones;
	}

	public void setOldOverWriteStones(int[] oldOverWriteStones) {
		this.oldOverWriteStones = oldOverWriteStones;
	}

	public int[] getOldBombs() {
		return oldBombs;
	}

	public void setOldBombs(int[] oldBombs) {
		this.oldBombs = oldBombs;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}


}
