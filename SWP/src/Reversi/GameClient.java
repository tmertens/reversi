package Reversi;

import java.io.*;
import Message.*;

/**
 * Sends and Receives bytes to/from the server
 * 
 */
public class GameClient {
	private java.net.Socket socket;

	/**
	 * @param server
	 *            server ip adress
	 * @param portnumber
	 *            server port
	 * @throws IOException
	 */
	public GameClient(String server, int portnumber) throws IOException {
		socket = new java.net.Socket(server, portnumber); // connection to server
	}

	/**
	 * sends byte array to the server
	 * 
	 * @param message
	 *            byte array
	 */
	public void sendByte(byte[] message) {
		try {
			DataOutputStream out = new DataOutputStream(
					socket.getOutputStream());
			out.write(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * reads byte array to the server
	 * 
	 * @return instance of class Message
	 */
	public Message readMessage() {
		try {
			DataInputStream in = new DataInputStream(socket.getInputStream());
			// get type and length of the message
			int messageType = in.readByte();
			int messageLength = in.readInt();

			// read bytes (with received length)
			byte[] messageBytes = null;
			if (messageLength > 0) {
				messageBytes = new byte[messageLength];
				in.read(messageBytes, 0, messageLength);
			}

			switch (messageType) {
			case 2: // read Map
				// convert to String
				String messageString = new String(messageBytes);
				// return instance of MessageMap
				return new MessageMap(2, messageLength, messageString);
			case 3: // read player number
				return new MessageNumber(3, 1, (int) messageBytes[0]);
			case 4: // request move
				// calculate time (4 bytes to int)
				int time = (int) (byteToInt(messageBytes[0]) << 24)
						+ (int) (byteToInt(messageBytes[1]) << 16)
						+ (int) (byteToInt(messageBytes[2]) << 8)
						+ (int) (byteToInt(messageBytes[3]));
				return new MessageRequest(4, 5, time, (int) messageBytes[4]);
			case 6: // read move
				// messageBytes index:
				// 1 - x coordinate
				// 3 - y coordinate
				// 4 - choice field
				// 5 - player number
				return new MessageMove(6, 6, (int) messageBytes[5],
						(int) messageBytes[1], (int) messageBytes[3],
						(int) messageBytes[4]);
			case 7: // read player disqualification
				return new MessageDisqualification(7, 1, (int) messageBytes[0]);
			case 8: // read next phase
				return new MessagePhase(8, 0);
			case 9: // end message
				return new MessageEnd(9, 0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * @param b
	 *            signed byte
	 * @return unsigned byte
	 */
	public static int byteToInt(byte b) {
		return b & 0xff;
	}

}
