package Reversi;

/**
 * Stores all important information about the players in local variables.
 * 
 */
public class Player {

	private int number;
	private int overWriteStones;
	private int bombs;

	/**
	 * Constructor
	 * 
	 * @param number
	 * @param bombs
	 * @param overWriteStones
	 */
	public Player(int number, int bombs, int overWriteStones) {
		this.setNumber(number);
		this.bombs = bombs;
		this.overWriteStones = overWriteStones;
	}

	public int getBombs() {
		return bombs;
	}

	public void setBombs(int bombs) {
		this.bombs = bombs;
	}

	public int getOverWriteStones() {
		return overWriteStones;
	}

	public void setOverWriteStones(int overWriteStones) {
		this.overWriteStones = overWriteStones;
	}

	public void decreaseOverWriteStones() {
		overWriteStones--;
	}

	public void increaseOverWriteStones() {
		setOverWriteStones(getOverWriteStones() + 1);
	}

	public void decreaseBombs() {
		setBombs(getBombs() - 1);
	}

	public void increaseBombs() {
		setBombs(getBombs() + 1);
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
