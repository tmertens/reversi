package Reversi;

/**
 * Stores the old FieldState of field (x,y) 
 *
 */
public class undoField {

	private int posX;
	private int posY;
	private FieldState oldState;
	
	/**
	 * Constructor 
	 * 
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param state old state
	 */
	public undoField(int x, int y, FieldState state){
		this.posX=x;
		this.posY=y;
		this.oldState=state;
	}
	
	
	public int getPosX() {
		return posX;
	}
	
	public void setPosX(int posX) {
		this.posX = posX;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	public FieldState getOldState() {
		return oldState;
	}
	
	public void setOldState(FieldState oldState) {
		this.oldState = oldState;
	}
	
	public boolean equals(Object u) {
		return (this.getPosX() == ((undoField)u).getPosX()) && (this.getPosY() == ((undoField)u).getPosY());
	}

	public int hashCode(){
		int index=this.getPosX()*100+this.getPosY();
		return index;
		
	}
	
}
