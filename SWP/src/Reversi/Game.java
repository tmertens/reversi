package Reversi;

/**
 * Saves all information about the current game and executes moves.
 * 
 */
public class Game {

	// Constants
	public static final int CHOICE_BOMB = 20;
	public static final int CHOICE_OVERWRITESTONE = 21;

	private boolean Disqualified = false;
	private boolean Ended = false;

	private Map map;

	private static Player[] players; // AI players
	private static int ownNumber;
	private static int phase;

	/**
	 * Loads map into data structure
	 * 
	 * @param filename
	 *            Filename of the map
	 * @see Map
	 */
	public Game(String filename) {
		this.map = new Map(filename);
		phase = 1;

		// Initialize player with default stone color
		players = new Player[map.getPlayers() + 1];
		for (int i = 1; i <= map.getPlayers(); i++) {
			players[i] = new Player(i, map.getBombs(), map.getOverwrite());
		}
	}

	/**
	 * Makes move taking into account the different scenarios in which a move
	 * can happen. Calls makeMoveNormal, makeMoveBomb and handles special
	 * stones.
	 * 
	 * @param x
	 *            x position of new stone
	 * @param y
	 *            y position of new stone
	 * @param player
	 *            player doing the turn
	 * @param type
	 *            type of the new stone
	 * @param choice
	 *            opponent player that player player wants to swap stones with
	 * @return true, if the move is successfully executed (including validation)
	 */
	public void makeMove(int x, int y, int player, int choice) {
		map.makeMove(x, y, player, choice);
	}	
	
	/**
	 * Returns the current player, which belongs to the color on the map
	 * 
	 * @param color
	 * @return Player
	 */
	public static Player getPlayer(int number) {
		return Game.players[number];
	}

	public Map getMap() {
		return map;
	}
	
	public static int getPhase() {
		return phase;
	}

	public static void setPhase(int phase) {
		Game.phase = phase;
	}

	public static int getOwnNumber() {
		return ownNumber;
	}

	public static void setOwnNumber(int number) {
		ownNumber = number;
	}

	public boolean getEnded() {
		return Ended;
	}
	
	public void setEnded(boolean ended) {
		Ended = ended;
	}

	public boolean getDisqualified() {
		return Disqualified;
	}

	public void setDisqualified(boolean disqualified) {
		Disqualified = disqualified;
	}

	public String toString() {
	
		String result = "";
		for (int y = 0; y < map.getSizeY(); y++) {
			for (int x = 0; x < map.getSizeX(); x++) {
				result = result + map.getField(x, y).toString() + " ";
			}
			result = result + "\n";
		}
	
		return result;
	
	}

}
