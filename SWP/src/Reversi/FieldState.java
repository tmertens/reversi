package Reversi;

/**
 * Enumeration representing different possible states of a field.
 * 
 */
public enum FieldState {
	PLAYER1, PLAYER2, PLAYER3, PLAYER4, PLAYER5, PLAYER6, PLAYER7, PLAYER8, NORMAL, BLOCKED, CHOICE, INVERSION, BONUS, EXPANSION
}
