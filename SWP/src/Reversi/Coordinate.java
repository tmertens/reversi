package Reversi;

/**
 * Helper class to simplify working with coordinates on the map.
 * 
 */
public class Coordinate implements Comparable<Coordinate> {
	private int x;
	private int y;
	private int rating;
	private int index;

	/**
	 * Creates new coordinate
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Creates new coordinate
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @param rating
	 *            move rating
	 */
	public Coordinate(int x, int y, int rating) {
		this.x = x;
		this.y = y;
		this.rating = rating;
	}

	/**
	 * Creates new coordinate
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @param rating
	 *            move rating
	 * @param index
	 *            sorting index
	 */
	public Coordinate(int x, int y, int rating, int index) {
		this.x = x;
		this.y = y;
		this.rating = rating;
		this.setIndex(index);
	}

	
	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String toString() {
		return x + "/" + y + "/" + rating;
	}

	public int hashCode() {
		int index = this.getX() * 100 + this.getY();
		return index;
	}

	public int compareTo(Coordinate o) {
		if (this.getRating() < o.getRating())
			return -1;
		if (this.getRating() > o.getRating())
			return 1;
	
		return 0;
	}

	public boolean equals(Object o) {
		return (this.getX() == ((Coordinate) o).getX()
				&& this.getY() == ((Coordinate) o).getY()
				&& this.getRating() == ((Coordinate) o).getRating());
	}

}