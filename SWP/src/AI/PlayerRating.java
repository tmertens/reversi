package AI;

import Reversi.Map;

/**
 * Combines the player number with the corresponding stonecount
 * 
 */
public class PlayerRating implements Comparable<PlayerRating> {
	private int player;
	private int stones;

	/**
	 * Constructor
	 * 
	 * @param player
	 *            player number
	 */
	public PlayerRating(int player) {
		this.player = player;
		this.stones = Map.getStoneCount(player);
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getStones() {
		return stones;
	}

	public void setStones(int stones) {
		this.stones = stones;
	}

	public int compareTo(PlayerRating o) {
		if (this.getStones() < o.getStones())
			return 1;
		if (this.getStones() > o.getStones())
			return -1;

		return 0;
	}

	public boolean equals(Object c) {
		return (this.getPlayer() == ((PlayerRating) c).getPlayer());
	}
}
