package AI;

import java.util.LinkedList;

import Reversi.Coordinate;
import Reversi.Field;
import Reversi.FieldState;
import Reversi.Game;
import Reversi.Map;
import Reversi.StoneType;

/**
 * Contains special algorithms for the AI
 * 
 */
public class AI {

	private static int score;
	private static int stonesAverage;
	private static int safeStones = 0;
	private static int halfSafeStones = 0;
	private static LinkedList<Coordinate> visited;
	private static LinkedList<Integer> target;

	/**
	 * Rates a map for a given player
	 * 
	 * @param map
	 *            given map
	 * @param player
	 *            player
	 * @return the map rating
	 */
	public static int rateMap(Map map, int player, int moveStoneCount) {
		if (Game.getPhase() == 1) {
			// Moving phase

			score = 0;
			safeStones = 0;
			halfSafeStones = 0;
			int avoidFields = 0;
			
			// Calculate safeStones
			for (Coordinate c : Map.getSafeStones()) {
				if (map.getField(c.getX(), c.getY()).getPlayer() == player) {
					safeStones++;
				}else if (map.getField(c.getX(), c.getY()).getPlayer() > 0) {
					safeStones--;
				}else{
									
					for (int i = 0; i < 8; i++) {
						if ((map.getFieldNeighbour(c.getX(), c.getY(), i) == null)||(map.getFieldNeighbour(c.getX(),c.getY(),i).getState() == FieldState.BLOCKED)) {
							//Do nothing
						}else if(map.getField(c.getX(), c.getY()).getPlayer() == player){
							avoidFields++;
						}
					}
					
				}
			}
			for (Coordinate c : Map.getSafeStones()) {
				if (map.getField(c.getX(), c.getY()).getPlayer() == player) {
					halfSafeStones++;
				}else if (map.getField(c.getX(), c.getY()).getPlayer() > 0) {
					halfSafeStones--;
				}
			}
			
			
			//Count winable Stones
			LinkedList<Coordinate> possibleMoves = Minimax.CalculateNormalMoves(player, 0, 0);
			
			score += 5*maxEffect(possibleMoves).getRating();
			score += sumEffect(possibleMoves);
			
			for(int i=1; i<=map.getPlayers(); i++){
				if(i!=player && i!=Game.getOwnNumber())score-=3*Minimax.CalculateNormalMoves(i, 0, 0).size();
			}
			
			score+=3*map.getPlayers()*Minimax.CalculateNormalMoves(player, 0, 0).size();
			
			score += (Map.getStoneCount())[player];
			score += 5*moveStoneCount;
			score += 100*safeStones;
			score += 3*halfSafeStones;
			score -= 200*avoidFields;
			
			score += 200*Game.getPlayer(Game.getOwnNumber()).getOverWriteStones();
			
			/*int nextplayer;
			possibleMoves.clear();
			if(player+1>map.getPlayers()){
				nextplayer = 1;
				possibleMoves = Minimax.CalculateNormalMoves(nextplayer, 0);
				//if(possibleMoves.isEmpty())
				//	possibleMoves.addAll(Minimax.CalculateAllMoves(1,0));
			}else{
				nextplayer = player + 1;
				possibleMoves = Minimax.CalculateNormalMoves(nextplayer, 0);
				//if(possibleMoves.isEmpty())
				//	possibleMoves.addAll(Minimax.CalculateAllMoves(player+1,0));
			}
			if(nextplayer!=player){
				score -= 10*maxEffect(possibleMoves).getRating();
			}*/	
			
			// Find current position
			/*LinkedList<PlayerRating> rating = Minimax.SortPlayers();
			int position = 0;
			int i;
			for (i = 0; i < rating.size(); i++) {
				if (rating.get(i).getPlayer() == player)
					position = i + 1;
			}
			score += 10*(10 - position);*/
			
			/*
			score = 0;
			safeStones = 0;
			halfSafeStones = 0;
			int nextplayer = 0;

			// Calculate safeStones
			for (Coordinate c : Map.getSafeStones()) {
				if (map.getField(c.getX(), c.getY()).getPlayer() == player) {
					safeStones++;
				}
			}
			for (Coordinate c : Map.getHalfSafeStones()) {
				if (map.getField(c.getX(), c.getY()).getPlayer() == player) {
					halfSafeStones++;
				}
			}

			// Calculates average stone count
			stonesAverage = 0;
			for (int i = 1; i <= map.getPlayers(); i++) {
				stonesAverage += (Map.getStoneCount())[i];
			}
			stonesAverage = (int) (stonesAverage / map.getPlayers());

			// Calculates score
			score = (Map.getStoneCount())[player] - stonesAverage + 10*safeStones
					+ halfSafeStones;
			score += moveStoneCount;
			
			//Count Overwritestones
			if(player == Game.getOwnNumber())
				score += 100*Game.getPlayer(player).getOverWriteStones();
			else
				score -= 100*Game.getPlayer(player).getOverWriteStones();
			
			//Count winable Stones
			LinkedList<Coordinate> possibleMoves = Minimax.CalculateNormalMoves(player, 0);
			//possibleMoves.addAll(Minimax.CalculateAllMoves(player,0));
			score += 10*maxEffect(possibleMoves).getRating();
			score += sumEffect(possibleMoves);
			
			possibleMoves.clear();
			if(player+1>map.getPlayers()){
				nextplayer = 1;
				possibleMoves = Minimax.CalculateNormalMoves(nextplayer, 0);
				//if(possibleMoves.isEmpty())
				//	possibleMoves.addAll(Minimax.CalculateAllMoves(1,0));
			}else{
				nextplayer = player + 1;
				possibleMoves = Minimax.CalculateNormalMoves(nextplayer, 0);
				//if(possibleMoves.isEmpty())
				//	possibleMoves.addAll(Minimax.CalculateAllMoves(player+1,0));
			}
			score -= maxEffect(possibleMoves).getRating();
			score += 100;
			
			//Minimazie moves possibilities for next players
			if(nextplayer == Game.getOwnNumber())
				score += possibleMoves.size();
			else
				score -= possibleMoves.size();
			
			
			*/
			
			/*
			//Count inversion
			int inversion=0;
			for(int x=0; x<map.getSizeX(); x++){
				for(int y=0;y<map.getSizeY(); y++){
					if(map.getField(x, y).getState()==FieldState.INVERSION){
						inversion++;
					}
				}
			}
			if((inversion % map.getPlayers())==0)
				score +=10;
			else
				score -= (inversion % map.getPlayers())*10;
			*/
			
		} else {
			// Bombing phase

			// Calculates average stone count
			stonesAverage = 0;
			for (int i = 1; i <= map.getPlayers(); i++) {
				if (target.contains(i)) {
					stonesAverage += (Map.getStoneCount())[i];
				} 
			}
			
			score = (Map.getStoneCount())[player] - stonesAverage;
		}

		return score;
	}

	/**
	 * @param move
	 *            LinkedList
	 * @return The Object with the most positive Effect
	 */
	
	 private static Coordinate maxEffect(LinkedList<Coordinate> move) {
	 
	 if(move.isEmpty())return new Coordinate(0,0,0);
	 
	 Coordinate max = move.getFirst();
	 
	 for (Coordinate x : move) { 
		 if (x.getRating() > max.getRating()) max = x;
	 }
	 
	 return max; }
	 

	/**
	 * @param move
	 *            LinkedList
	 * @return the sum of all Effects in given List
	 */
	
	 private static int sumEffect(LinkedList<Coordinate> move) { 
		 int sum = 0;
		 for (Coordinate x : move) { 
			 sum = sum + x.getRating(); 
		 }
		 return sum; 
	 }
	 

	/**
	 * @param move
	 *            LinkedList
	 * @return The Object with the most negative Effect
	 */
	/*
	 * private static Coordinate minEffect(LinkedList<Coordinate> move) {
	 * 
	 * if(move.isEmpty())return new Coordinate(0,0,0);
	 * 
	 * Coordinate min = move.getFirst();
	 * 
	 * for (Coordinate x : move) { if (x.getEffect() < min.getEffect()) min = x;
	 * }
	 * 
	 * return min; }
	 */

	/**
	 * Tests if a move is valid using moveDirAI.
	 * 
	 * @param x
	 *            x position of new stone
	 * @param y
	 *            y position of new stone
	 * @param player_move
	 *            player doing the turn
	 * @param player_calc
	 *            player doing the calculation
	 * @param type
	 *            type of the new stone
	 * @param map
	 *            the board which is being calculated
	 * @return if given move is valid
	 */
	public static int isValidMoveTrivial(int x, int y, int player,
			StoneType type, Map map) {

		int rating;
		visited = new LinkedList<Coordinate>();

		switch (type) {
		case NORMAL:
			// Search in all directions
			rating = 0;
			for (int i = 0; i < 8; i++) {
				rating += moveDirAI(x, y, player, i, 0, map);
			}
			if (rating > 0)
				return rating;
			break;
		case OVERWRITE:
			if (map.getField(x, y).getState() == FieldState.EXPANSION) {
				// Expansion rule
				return 1;
			} else if (map.getField(x, y).getPlayer() > 0) {
				// Search in all directions
				rating = 0;
				for (int i = 0; i < 8; i++) {
					rating += moveDirAI(x, y, player, i, 0, map);
				}
				if (rating > 0)
					return rating;
			}
			break;
		default:
			break;
		}

		return 0;
	}

	/**
	 * Attempts to move in a specific direction. Used for testing possible
	 * moves.
	 * 
	 * @param x
	 *            x position of the new stone
	 * @param y
	 *            y position of the new stone
	 * @param player_move
	 *            player doing the turn
	 * @param player_calc
	 *            player doing the calculation
	 * @param dir
	 *            direction to move in
	 * @param counter
	 *            recursion depth
	 * @param map
	 *            the board which is being calculated
	 * @return true on success, false otherwise
	 */
	public static int moveDirAI(int x, int y, int player, int dir, int counter,
			Map map) {

		Field fieldNeighbour;
		visited.clear();
		Coordinate c;
		visited.add(new Coordinate(x, y));

		while (true) {
			fieldNeighbour = map.getFieldNeighbour(x, y, dir);

			if (fieldNeighbour == null)
				break;

			c = new Coordinate(fieldNeighbour.getPosX(),
					fieldNeighbour.getPosY());

			if (visited.contains(c))
				return 0;
			visited.add(c);

			if (fieldNeighbour.getPlayer() == player) {
				if (counter == 0)
					break;
				return counter;
			} else if (fieldNeighbour.getPlayer() > 0
					|| (fieldNeighbour.getState() == FieldState.EXPANSION)) {
			} else {
				break;
			}
			dir = map.getField(x, y).getDirection(dir);
			x = fieldNeighbour.getPosX();
			y = fieldNeighbour.getPosY();

			counter++;
		}

		return 0;
	}

	/**
	 * 
	 * @param list
	 *            list of Integers
	 * @return sum of all elements
	 */
	public static int sumInt(LinkedList<Integer> list) {
		int sum = 0;

		for (int x : list) {
			sum = sum + x;
		}

		return sum;
	}

	/**
	 * Calculates best player for choice fields
	 * 
	 * @param map
	 *            map with choice field
	 * @param player
	 *            player who sets stone on the choice field
	 * @return best player to change
	 */
	public static int getBestChoice(Map map, int player) {

		int bestPlayer = 0;
		int bestValue = -10000;
		int rating = 0;

		for (int i = 1; i <= map.getPlayers(); i++) {
			rating = AI.rateMap(map, i, 0);
			if (rating > bestValue) {
				bestValue = rating;
				bestPlayer = i;
			}
		}

		return bestPlayer;
	}

	
	public static LinkedList<Integer> getTarget() {
		return target;
	}

	public static void setTarget(LinkedList<Integer> target) {
		AI.target = target;
	}

}
