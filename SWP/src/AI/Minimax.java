package AI;

import Benchmarks.Table;
import Reversi.*;

import java.util.LinkedList;

/**
 * Contains the Minimax algorithm and corresponding functions
 * 
 */
public class Minimax {

	private static int startingDepth;
	private static Coordinate bestMove;
	private static Coordinate currentBestMove;
	private static int amount;
	private static long timePerMove;
	public static long moveBegin;
	private static int index;
	public static Map globalMap = null;
	private static boolean timeIsUp = false;
	private static int windowOffset = -1; // -1 -> has not been calculated yet
	private static int finalPlayer;
	
	// Constants
	public static final long timeBuffer = 1000; // time in ms to complete
												// calculation and send move
	public static final int preSortingFilter = 2; // minimum depth in order to
													// use preSorting for the
													// possible moves
	
	private static int[] time = {491,1949,1844,1862,1869};
	
	/**
	 * Executes the minimax algorithm
	 * 
	 * @param map
	 *            current map
	 * @param depth
	 *            defined search depth
	 */
	public static void minimax(Map map, int depth) {

		globalMap = map;

		// Initialize timing for iterative deepening
		moveBegin = System.currentTimeMillis();

		amount = 0;

		int alpha;
		int beta;

		resetBestMove();
		resetCurrentBestMove();
		
		// Begin timing
		if (Reversi.benchmarkingEnabled)
			Table.getInstance().beginState();
		// Depth = 1 in bombingphase
		if (Game.getPhase() == 2)
			depth = 1;
		// Start iterative deepening
		int iterativeDepth = 1;
		
		//Find player number after all inversions
		
		finalPlayer = Game.getOwnNumber();
		if((map.reachableInversion() % map.getPlayers()) != 0){
			int shift = (map.reachableInversion() % map.getPlayers());
			while (shift > 0){
				finalPlayer = (finalPlayer % map.getPlayers()) + 1;
				shift --;
			}
			depth = 1;
		}		
		
		//finalPlayer=Game.getOwnNumber();
		
		while (!checkTime(iterativeDepth) && iterativeDepth <= depth) {
			setStartingDepth(iterativeDepth);
			resetCurrentBestMove();

			// Aspiration Windows
			if (iterativeDepth == 1) {
				alpha = Integer.MIN_VALUE;
				beta = Integer.MAX_VALUE;
			} else {
				alpha = bestMove.getRating() - 2*getWindowOffset();
				beta = bestMove.getRating() + 2*getWindowOffset();
			}

			// Execute Minimax
			minimaxMax(iterativeDepth, Game.getOwnNumber(), alpha, beta, 0);

			// Window too narrow, repeat minimax
			if (currentBestMove == null) {
				minimaxMax(iterativeDepth, Game.getOwnNumber(),Integer.MIN_VALUE, Integer.MAX_VALUE, 0);
			}
			if(depth==1)
				bestMove = new Coordinate(currentBestMove.getX(),currentBestMove.getY(), currentBestMove.getRating());
			if (checkTime(0))
				break;

			// Set best Move
			bestMove = new Coordinate(currentBestMove.getX(),currentBestMove.getY(), currentBestMove.getRating());
			iterativeDepth++;
		}

		// Finish timing
		if (Reversi.benchmarkingEnabled)
			Table.getInstance().submitState(amount);
	}

	/**
	 * max part of the minimax algorithm
	 * 
	 * @param map
	 *            current map
	 * @param depth
	 *            remaining depth
	 * @param player
	 *            current player
	 * @param alpha
	 *            used for pruning
	 * @param beta
	 *            used for pruning
	 * @return rating current map rating
	 */
	public static int minimaxMax(int depth, int player, int alpha, int beta, int moveStoneCount) {

		int maxValue = alpha;
		int nextPlayer = (player % globalMap.getPlayers()) + 1; // Select next
																// player
		int nextDepth = depth - 1; // Reduce depth
		int value;
		int old_pos;
		int pos;
		LinkedList<Coordinate> possibleMoves = CalculateNormalMoves(player,1, moveStoneCount);

		if (possibleMoves.isEmpty())
			possibleMoves = CalculateAllMoves(player,1, moveStoneCount);

		if (possibleMoves.isEmpty())
			return AI.rateMap(globalMap, finalPlayer, 0);

		// Sort moves
		if (Reversi.isPreSortingMoves() && (depth >= preSortingFilter)) {
			java.util.Collections.sort(possibleMoves);
		}

		Coordinate tmp = possibleMoves.getLast();

		while (!possibleMoves.isEmpty() && !checkTime(0)) {
			amount++;

			// Do move and save information to undo move later
			undoInformation undo = globalMap.makeMove(possibleMoves.getLast().getX(), possibleMoves.getLast().getY(), player, -1);

			if (nextDepth == 0) {
				value = possibleMoves.getLast().getRating();
			} else {
				value = minimaxMin(nextDepth, nextPlayer, maxValue, beta, moveStoneCount+nextDepth*undo.getMoveList().size()); // DFS search
			}

			// Undo move
			globalMap.undoMove(undo);

			// Check old sorting
			if (value == maxValue) {
				old_pos = tmp.getIndex();
				pos = possibleMoves.getLast().getIndex();

				if (old_pos > pos) {
					possibleMoves.remove(possibleMoves.getLast());
					continue;
				}
			}
			// Search maximum
			if (value >= maxValue) {
				maxValue = value;
				tmp = possibleMoves.getLast();

				// beta pruning
				if (maxValue > beta)
					if (Reversi.isAlphaBetaEnabled())
						break;

				if (depth == startingDepth) {
					currentBestMove = new Coordinate(possibleMoves.getLast()
							.getX(), possibleMoves.getLast().getY(), maxValue);
				}
			}

			// Remove last move from list
			possibleMoves.remove(possibleMoves.getLast());
		}
		return maxValue;
	}

	/**
	 * min part of the minimax algorithm
	 * 
	 * @param map
	 *            current map
	 * @param depth
	 *            remaining depth
	 * @param player
	 *            current player
	 * @param alpha
	 *            used for pruning
	 * @param beta
	 *            used for pruning
	 * @return rating current map rating
	 */
	public static int minimaxMin(int depth, int player, int alpha, int beta, int moveStoneCount) {

		int minValue = beta;
		int nextPlayer = (player % globalMap.getPlayers()) + 1; // Select next
																// player
		int nextDepth = depth - 1; // Reduce depth
		int old_pos;
		int pos;
		LinkedList<Coordinate> possibleMoves = CalculateNormalMoves(player,1, moveStoneCount);

		if (possibleMoves.isEmpty())
			possibleMoves = CalculateAllMoves(player,1,moveStoneCount);

		if (possibleMoves.isEmpty())
			return AI.rateMap(globalMap, finalPlayer,0);

		// Sort moves
		if (Reversi.isPreSortingMoves() && (depth >= preSortingFilter)) {
			java.util.Collections.sort(possibleMoves);
		}

		Coordinate tmp = possibleMoves.getFirst();

		while (!possibleMoves.isEmpty() && !checkTime(0)) {
			amount++;

			// Do move and save information to undo move later
			undoInformation undo = globalMap.makeMove(possibleMoves.getFirst()
					.getX(), possibleMoves.getFirst().getY(), player, -1);

			int value = 0;

			if (nextDepth == 0) {
				value = possibleMoves.getFirst().getRating();
			} else if (nextPlayer == Game.getOwnNumber()) { // Own player tries to maximize rating
				value = minimaxMax(nextDepth, nextPlayer, alpha, minValue, moveStoneCount-undo.getMoveList().size()); // DFS search
			} else { // all the other player try to minimize our rating
				value = minimaxMin(nextDepth, nextPlayer, alpha, minValue, moveStoneCount-undo.getMoveList().size()); // DFS search
			}

			// Undo move
			globalMap.undoMove(undo);

			// Check old sorting
			if (value == minValue) {
				old_pos = tmp.getIndex();
				pos = possibleMoves.getFirst().getIndex();

				if (old_pos < pos) {
					possibleMoves.remove(possibleMoves.getFirst());
					continue;
				}
			}
			// Search maximum
			if (value <= minValue) {
				minValue = value;
				tmp = possibleMoves.getFirst();
				// alpha pruning
				if (minValue < alpha)
					if (Reversi.isAlphaBetaEnabled())
						break;
			}
			// Remove first move from list
			possibleMoves.remove(possibleMoves.getFirst());
		}
		return minValue;
	}

	/**
	 * Calculates all possible moves of the player
	 * 
	 * @param map
	 *            current map
	 * @param player
	 *            current player
	 * @return all possible moves of the player as Coordinate list
	 */
	public static LinkedList<Coordinate> CalculateAllMoves(int player, int mode, int moveStoneCount) {
		index = 0;

		LinkedList<Coordinate> moves = new LinkedList<Coordinate>();
		int rating;
		Coordinate c;
		undoInformation undo = null;

		if (Game.getPhase() == 1) {
			for (int x = 0; x < globalMap.getSizeX(); x++) {
				for (int y = 0; y < globalMap.getSizeY(); y++) {
					if (!checkTime(0)) {
						if (globalMap.getField(x, y).getState() != FieldState.BLOCKED) {
							// Normal stones
							if (globalMap.getField(x, y).getPlayer() == 0 && globalMap.getField(x, y).getState() != FieldState.EXPANSION) {
							
							} else {
								// Overwrite stones
								if (player == Game.getOwnNumber() && Game.getPlayer(player).getOverWriteStones() > 0) {
									rating = AI.isValidMoveTrivial(x, y,player, StoneType.OVERWRITE,globalMap);
									if (rating > 0) {
										c = new Coordinate(x, y);

										// undo.getMoveList().size() represents
										// the number of new stones with this
										// move\r
										if(mode == 1){
											// Calculate rating for sorting
											undo = globalMap.makeMove(x, y, player,1);
											if (player == Game.getOwnNumber()) {
												rating = AI.rateMap(globalMap, finalPlayer,	moveStoneCount+undo.getMoveList().size());
											} else {
												rating = AI.rateMap(globalMap,finalPlayer,moveStoneCount-undo.getMoveList().size());
											}
											globalMap.undoMove(undo);
										}									

										c.setRating(rating);
										c.setIndex(index);
										moves.add(c);
										index++;
									}
								}
							}
						}
					}
				}
			}
		} else {
			if (Game.getPlayer(player).getBombs() > 0) {
				moves = FindBestBombMove(player);
			}
		}
		return moves;
	}

	/**
	 * Calculates all possible moves with a normal stone of the player
	 * 
	 * @param map
	 * @param player
	 * @return all possible moves as Coordinate list
	 */
	public static LinkedList<Coordinate> CalculateNormalMoves(int player, int mode, int moveStoneCount) {
		index = 0;
	
		LinkedList<Coordinate> moves = new LinkedList<Coordinate>();
		int rating = 0;
		int rating_stone;
		Coordinate c;
		undoInformation undo = null;
	
		if (Game.getPhase() == 1) {
			for (int x = 0; x < globalMap.getSizeX(); x++) {
				for (int y = 0; y < globalMap.getSizeY(); y++) {
					if (!checkTime(0)) {
						if (globalMap.getField(x, y).getState() != FieldState.BLOCKED) {
							// Normal stones
							if (globalMap.getField(x, y).getPlayer() == 0
									&& globalMap.getField(x, y).getState() != FieldState.EXPANSION) {
								rating_stone = AI.isValidMoveTrivial(x, y, player,
										StoneType.NORMAL, globalMap);
								
								if (rating_stone > 0) {
									c = new Coordinate(x, y);
									// Calculate rating for sorting

									// undo.getMoveList().size() represents the
									// number of new stones with this move\r
									if(mode == 1){
										int choicePlayer = 1;
										if(globalMap.getField(x, y).getState() == FieldState.CHOICE){
											choicePlayer = AI.getBestChoice(globalMap, Game.getOwnNumber());
										}
										if(globalMap.getField(x, y).getState() == FieldState.BONUS){
											choicePlayer = 21;
										}
										
										undo = globalMap.makeMove(x, y, player, choicePlayer);
										if (player == Game.getOwnNumber()) {
											rating = AI.rateMap(globalMap, finalPlayer, moveStoneCount+undo.getMoveList().size());
										} else {
											rating = AI.rateMap(globalMap, finalPlayer, moveStoneCount-undo.getMoveList().size());
										}
										globalMap.undoMove(undo);
									}
									
									c.setRating(rating+rating_stone);
									c.setIndex(index);
									moves.add(c);
									index++;
								}
							}
						}
					}
				}
			}
		}
		return moves;
	}

	/**
	 * Pre-select the possible bomb moves
	 * 
	 * @param player
	 *            current player
	 * @return list of useful bomb moves
	 */
	private static LinkedList<Coordinate> FindBestBombMove(int player) {
		
		int position = 0;

		// Make rating
		LinkedList<PlayerRating> rating = SortPlayers();
		position = 0;
		int i;
		for (i = 0; i < rating.size(); i++) {
			if (rating.get(i).getPlayer() == player)
				position = i + 1;
		}

		// Set target players
		LinkedList<Integer> target = new LinkedList<Integer>();
		if (position > 4) {
			for (i = 4; i < position; i++) {
				target.add(new Integer(rating.get(i - 1).getPlayer()));
			}
		} else if (position == 1) {
			if (rating.size() > 1)
				target.add(new Integer(rating.get(1).getPlayer()));
			if (rating.size() > 2)
				target.add(new Integer(rating.get(2).getPlayer()));
		} else {
			for (i = 1; i < position; i++) {
				target.add(new Integer(rating.get(i - 1).getPlayer()));
			}
		}

		LinkedList<Coordinate> moves = new LinkedList<Coordinate>();
		for (int x = 0; x < globalMap.getSizeX(); x++) {
			for (int y = 0; y < globalMap.getSizeY(); y++) {
				if (!checkTime(0)) {
					// Bombs
					int tmp = 0;
					if (globalMap.getField(x, y).getState() == FieldState.BLOCKED)
						continue;

					if (globalMap.getBombSize() > 0) {
						// Look in all directions
						for (int dir = 0; dir < 8; dir++) {
							if (globalMap.getField(x, y).getNeighbour(dir) != null) {
								if (target.contains(globalMap.getField(x, y)
										.getNeighbour(dir).getPlayer())) {
									tmp = tmp + 10;
								} else if (globalMap.getField(x, y)
										.getNeighbour(dir).getPlayer() == player) {
									tmp = tmp - 10;
								} else {
									tmp++;
								}
							}
						}
					}

					if (globalMap.getField(x, y).getPlayer() == player) {
						// Field is our field
						tmp = tmp - 10;
					} else {
						if (target.contains(globalMap.getField(x, y).getPlayer())) {
							tmp = tmp + 10;
						} else {
							tmp++;
						}
					}				
					moves.add(new Coordinate(x, y, tmp));
				}
			}
		}
		AI.setTarget(target);
		return moves;
	}

	/**
	 * Estimation of used time for given depth, if depth=0 trivial check
	 * @param depth 
	 * @return if time is up
	 */
	public static boolean checkTime(int depth) {
		// Time is up when difference between latest possible point in time to
		// deliver a move (moveBegin+timePerMove) and current time is negative

		if(depth == 0)
			timeIsUp = !((System.currentTimeMillis() - moveBegin + timeBuffer < timePerMove));
		else if(depth < time.length)
			timeIsUp = !(timePerMove - (timeBuffer + (System.currentTimeMillis() - moveBegin))> time[depth-1]);
		else
			timeIsUp = !(timePerMove - (timeBuffer + (System.currentTimeMillis() - moveBegin)) > time[time.length-1]);
		if (Reversi.ignoreTime)
			timeIsUp = false;
		return timeIsUp;
	}

	/**
	 * @return a sorted list of players
	 */
	public static LinkedList<PlayerRating> SortPlayers() {

		LinkedList<PlayerRating> rating = new LinkedList<PlayerRating>();
		for (int i = 1; i < globalMap.getPlayers() + 1; i++) {
			rating.add(new PlayerRating(i));
		}

		java.util.Collections.sort(rating);
		return rating;
	}

	/**
	 * @return the windowOffset
	 */
	public static int getWindowOffset() {
		if (windowOffset == -1) {
			// Offset has not been calculated yet
			int numberOfFields = globalMap.getSizeX()*globalMap.getSizeY();
			windowOffset=(int) (Math.abs(0.12317156560522342 * numberOfFields
					- 0.00010406618087741973 * Math.pow(numberOfFields, 2)
					+ 0.000000025768790940954657 * Math.pow(numberOfFields, 3)));
		}
		return windowOffset;
	}
	
	private static void resetBestMove() {
		bestMove = null;
	}

	private static void resetCurrentBestMove() {
		currentBestMove = null;
	}
	
	public static Coordinate getCurrentbestMove() {
		return currentBestMove;
	}

	public static void setCurrentbestMove(Coordinate currentBestMove) {
		Minimax.currentBestMove = currentBestMove;
	}

	public static long getTimePerMove() {
		return timePerMove;
	}

	public static void setTimePerMove(long timePerMove) {
		Minimax.timePerMove = timePerMove;
	}

	public static int getStartingDepth() {
		return startingDepth;
	}

	public static void setStartingDepth(int startingDepth) {
		Minimax.startingDepth = startingDepth;
	}

	public static Coordinate getBestMove() {
		return bestMove;
	}

	public static void setBestMove(Coordinate move) {
		bestMove = move;
	}

	public static int getFinalPlayer() {
		return finalPlayer;
	}

	public void setFinalPlayer(int finalPlayer) {
		Minimax.finalPlayer = finalPlayer;
	}

}
