package Message;

/**
 * Extends Message
 * Saves number of disqualified player
 * 	
 */
public class MessageDisqualification extends Message{

	private int number;
	
	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 * @param player number
	 */
	public MessageDisqualification(int type, int length, int number) {
		super(type, length);
	
		this.number=number;
	}


	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	
}
