package Message;
/**
 * Abstract class to save general information about server messages
 *
 */
public abstract class Message {
	
	private int type;
	private int length;
	
	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 */
	public Message(int type, int length){
		this.type=type;
		this.length=length;
	}
	
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}

	
}
