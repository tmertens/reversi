package Message;

/**
 * Extends Message
 * Signalizes end of the game
 */
public class MessageEnd extends Message{

	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 */
	public MessageEnd(int type, int length) {
		super(type, length);
	}
	
}
