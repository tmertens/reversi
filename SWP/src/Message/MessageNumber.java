package Message;

/**
 * Extends Message
 * Saves our own player number
 */
public class MessageNumber extends Message{

	private int number;
	
	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 * @param player number
	 */
	
	public MessageNumber(int type, int length, int number) {
		super(type, length);
	
		this.number=number;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	
}
