package Message;

/**
 * Extends Message
 * Signalizes next phase
 */
public class MessagePhase extends Message{

	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 */
	public MessagePhase(int type, int length) {
		super(type, length);
	}
	
}