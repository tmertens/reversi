package Message;

/**
 * Extends Message
 * Saves the maximum time and depth of our next move
 */
public class MessageRequest extends Message{

	private long time;
	private int depth;
	
	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 * @param time limit
	 * @param depth limit
	 */
	public MessageRequest(int type, int length, long time, int depth) {
		super(type, length);
	
		this.setTime(time);
		this.setDepth(depth);
	}

	
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}
	
}
