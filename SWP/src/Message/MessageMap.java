package Message;

/**
 * Extends Message
 * Contains the map as String
 */
public class MessageMap extends Message{

	String map;
	
	/**
	 * Constructor
	 * @param type message type
	 * @param length length of the String containing the map
	 * @param map map as String
	 */
	public MessageMap(int type, int length,String map){
		super(type, length);
		this.map=map;
	}
	
	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}
	
}
