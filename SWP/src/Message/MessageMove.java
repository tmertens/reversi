package Message;

/**
 * Extends Message
 * Saves all information about an executed move
 * 
 */
public class MessageMove extends Message{

	private int player;
	private int posX;
	private int posY;
	private int choice;
	
	/**
	 * Constructor 
	 * 
	 * @param type type number
	 * @param length length in bytes
	 * @param excecuting player
	 * @param X coordinate
	 * @param Y coordinate
	 * @param type of choice
	 * 
	 */
	
	public MessageMove(int type, int length, int player, int posX, int posY, int choice) {
		super(type, length);
		
		this.player=player;
		this.posX=posX;
		this.posY=posY;
		this.choice=choice;
	}

	
	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getChoice() {
		return choice;
	}

	public void setChoice(int choice) {
		this.choice = choice;
	}

}
