package Benchmarks;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class writes tables to file.
 * 
 */
public class TableManager {
	private String filename;
	private String gameName = "";
	private Table table;

	// DEFINES
	private static String docFolderName = "Benchmarks";
	private final static Charset ENCODING = StandardCharsets.UTF_8;

	/**
	 * Constructor that lays the necessary groundwork for filing the benchmarks
	 * 
	 * @param table
	 *            Table for this TableManager to manage
	 */
	public TableManager(Table table) {
		// Create benchmark folder if necessary
		String workingDir = System.getProperty("user.dir");
		String fileSeparator = System.getProperty("file.separator");
		File benchFolder = new File(workingDir + fileSeparator + docFolderName);
		benchFolder.mkdirs();

		// Pick new unique filename for this table
		if (Reversi.Reversi.automatic) {
			gameName = Reversi.Reversi.name;
		}

		filename = benchFolder.getAbsolutePath() + fileSeparator + gameName + generateDateString() + ".txt";

		// Create new blank table
		this.table = table;
	}

	/**
	 * Constructor
	 */
	public TableManager() {
		this(new Table());
	}

	/**
	 * Writes table managed by this TableManager to file
	 * 
	 * @return true if successful, false otherwise
	 */
	public boolean writeTableToFile() {
		try {
			Path path = Paths.get(filename);
			Files.write(path, table.getLines(), ENCODING);
			return true;
		} catch (Exception e) {
			System.out.println("Writing to file " + filename + " failed!");
			return false;
		}
	}

	/**
	 * @return date as String
	 */
	private static String generateDateString() {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
		String dateString = df.format(date);
		return dateString;
	}
}
