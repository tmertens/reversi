package Benchmarks;

import java.util.LinkedList;

/**
 * A table holds all the benchmark information of one run (one match) of the AI.
 * Table is a singleton, so there is only one instance.
 * 
 */
public class Table {
	private LinkedList<String> lines;
	private int moveID;
	private long compTimePerState;
	private long numOfStates;
	private long totalCompTime;
	private long startTime;
	private boolean alphaBetaEnabled;
	private boolean preSortingEnabled;
	private int maxDepth;

	private static Table instance;

	public Table() {
		lines = new LinkedList<String>();

		// Add first row a.k.a. head of the table
		lines.add("moveID, computation time per state, number of states, total computation time, alpha-beta is enabled?, Presorting enabled?, maxDepth");

		moveID = 0;
		compTimePerState = 0;
		numOfStates = 0;
		totalCompTime = 0;
		alphaBetaEnabled = Reversi.Reversi.isAlphaBetaEnabled();
		preSortingEnabled = Reversi.Reversi.isPreSortingMoves();
		startTime = 0;
		maxDepth = AI.Minimax.getStartingDepth();

		// Save this instance in static variable
		instance = this;
	}

	/**
	 * Called when entering a new state. Must be called before submitState()
	 */
	public void beginState() {
		// Start the timer
		startTime = System.nanoTime();
	}

	/**
	 * Called during move picking (in minimax)
	 */
	public void submitState(int amount) {
		totalCompTime += System.nanoTime() - startTime;
		numOfStates = amount;
	}

	/**
	 * Called in order to save gathered data to the table after a move has been
	 * picked.
	 */
	public void submitMove() {
		// Do some math
		if (numOfStates > 0)
			compTimePerState = totalCompTime / numOfStates;

		// Add data to table
		lines.add(moveID + ", " + compTimePerState + ", " + numOfStates + ", "
				+ totalCompTime + ", " + alphaBetaEnabled + ", "
				+ preSortingEnabled + ", " + maxDepth);

		// Increment moveID and reset all other counters
		moveID++;
		compTimePerState = 0;
		numOfStates = 0;
		totalCompTime = 0;
		alphaBetaEnabled = Reversi.Reversi.isAlphaBetaEnabled();
		preSortingEnabled = Reversi.Reversi.isPreSortingMoves();
		maxDepth = AI.Minimax.getStartingDepth();
	}
	
	
	public static Table getInstance() {
		return instance;
	}

	public LinkedList<String> getLines() {
		return lines;
	}
	
}
