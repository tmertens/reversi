

import static org.junit.Assert.*;
import org.junit.Test;

import Reversi.Coordinate;
import Reversi.Game;
import Reversi.Reversi;

/**
 * Junit test class
 *
 */
public class ReversiTest {
	Game game;
	Reversi reversi;
	public ReversiTest(){
		game=new Game("3\n1\n2 4\n5 5\n0 0 0 0 0\n0 1 2 0 0\n0 2 1 0 0\n0 1 2 0 0\n0 0 0 0 3\n");
		Game.setOwnNumber(1);
		reversi = new Reversi();
	}
	
	@Test
	public void MapTestPlayers() {
		assertEquals(3,game.getMap().getPlayers());	
	}
	@Test
	public void MapTestSizeX() {
		assertEquals(5,game.getMap().getSizeX());
	}
	@Test
	public void MapTestSizeY() {
		assertEquals(5,game.getMap().getSizeY());
	}
	@Test
	public void MapTestBombs() {
		assertEquals(2,game.getMap().getBombs());	
	}
	@Test
	public void MapTestBombSize() {
		assertEquals(4,game.getMap().getBombSize());
	}
	@Test
	public void MapTestOverwrite() {
		assertEquals(1,game.getMap().getOverwrite());	
	}
	@Test
	public void MapTestField0() {
		assertEquals(0,game.getMap().getField(0, 0).getPlayer());
	}
	@Test
	public void MapTestField1() {
		assertEquals(1,game.getMap().getField(1, 1).getPlayer());
	}
	@Test
	public void MapTestField2() {
		assertEquals(2,game.getMap().getField(1, 2).getPlayer());
	}
	@Test
	public void MapTestField3() {
		assertEquals(3,game.getMap().getField(4, 4).getPlayer());
	}
	
	@Test
	public void Alpha(){
		Reversi.alphaBetaEnabled=true;
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.minimax(game.getMap(), 2);
		Coordinate move = AI.Minimax.getBestMove();
		assertNotNull(move);
	}
	
	@Test
	public void NoAlpha(){
		Reversi.alphaBetaEnabled=false;
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.minimax(game.getMap(), 2);
		Coordinate move = AI.Minimax.getBestMove();
		assertNotNull(move);
	}
	
	@Test
	public void CompareAlphaAndNoAlpha(){
		Reversi.alphaBetaEnabled=true;
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.minimax(game.getMap(), 2);
		Coordinate move = AI.Minimax.getBestMove();
		
		Reversi.alphaBetaEnabled=false;
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.minimax(game.getMap(), 2);
		Coordinate move2 = AI.Minimax.getBestMove();
		boolean same=false;
		if(move.getX()==move2.getX() && move.getY()==move2.getY())
			same=true;
		
		assertTrue(same);
		
	}
	
	@Test
	public void ComparePreSortingAndNoPreSorting(){
		
		Reversi.preSortingMoves=true;
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.minimax(game.getMap(), 2);
		Coordinate move = AI.Minimax.getBestMove();
		
		Reversi.preSortingMoves=true;
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.minimax(game.getMap(), 2);
		Coordinate move2 = AI.Minimax.getBestMove();
		boolean same=false;
		if(move.getX()==move2.getX() && move.getY()==move2.getY())
			same=true;
		
		assertTrue(same);	
	}
	
	@Test
	public void CheckTime(){
		AI.Minimax.setTimePerMove(5000);
		AI.Minimax.moveBegin=System.currentTimeMillis();
		
		assertFalse(AI.Minimax.checkTime(0));
		
		while(System.currentTimeMillis()< (AI.Minimax.moveBegin+5000+AI.Minimax.timeBuffer) ){}
		
		assertTrue(AI.Minimax.checkTime(0));	
	}
	
	@Test
	public void CheckTimeMinimax(){
		
		double start=System.currentTimeMillis();
		
		AI.Minimax.setTimePerMove(35000);
		AI.Minimax.minimax(game.getMap(), 10);
		
		double end=System.currentTimeMillis();
		
		boolean usedTime=(end-start) < 35000;
		assertTrue(usedTime);
	}
	
}
