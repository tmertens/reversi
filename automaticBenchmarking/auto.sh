# Demonstriert die Verwendung einer while-Schleife
# awhile1
# Liste von Argumenten in einem Array speichern
# Version: Korn-Shell (auskommentiert)
#set -A array $*
# Version: bash

array=(2)
maps=(easy2.txt)


max=1
k=0
mind=2
maxd=5
cd test
while (( k < max ))
do
  depth=$mind
  while ((depth<=maxd))
  do
    konsole -e ./server_nogl ${maps[k]}
    sleep 1  
    konsole -e java -jar Reversi.jar -t true true true ${depth}
    sleep 1  
    i=${array[$k]}
    i=`expr $i - 1`
    while [ $i -gt 0 ]
    do
      konsole -e ./ai_trivial
      sleep 1  
      i=`expr $i - 1`
    done
    
    while ! [ -a alpha-pre.txt ]
    do
      sleep 5
    done
    echo 'ready';
    /bin/rm alpha-pre.txt
    depth=`expr $depth + 1`
  done

  k=`expr $k + 1`
done
